package dn.batch.reactproject.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import dn.batch.reactproject.model.User;
import dn.batch.reactproject.repository.UserRepo;

@CrossOrigin
@RestController
@RequestMapping("/")
public class UserController {
	
	@Autowired
	UserRepo userRepo;
	
	@GetMapping("/test")
	public List<User>  getAllUser(){
		return userRepo.findAll();
	}
	
//	REGISTER
	// kiểm tra trùng tên
	@PostMapping("/check-loginname")
	public ResponseEntity<String> checkLoginNameExists(@RequestBody String loginUser) {
		User userExists = userRepo.findByLoginUser(loginUser);
		if(userExists != null) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body("Trùng tên đăng nhập");
		}
		return ResponseEntity.ok(" Success");
	}
	// thêm vào db
	@PostMapping("/register")
	public ResponseEntity<String> registerUser(@RequestBody User user) {
		ResponseEntity<String> checkResult = checkLoginNameExists(user.getLoginUser());
		if (checkResult.getStatusCode() == HttpStatus.CONFLICT) {
			return checkResult;
		}
		userRepo.save(user);
		
		return ResponseEntity.ok(" Đã đăng kí");
	}
	
	//LOGIN
	private boolean CheckUser(String loginUser,String passwordUser) {
		User user = userRepo.findByLoginUser(loginUser);
		if(user == null) {
			return false;
		}
		if(user.getPasswordUser().equals(passwordUser)) {
			return true;
		}
		return false;
	}
	
	@PostMapping("/login")
	public ResponseEntity<String> doLogin(@RequestBody User user) {
		boolean isValid = CheckUser(user.getLoginUser(), user.getPasswordUser());
		if(isValid) {
			return ResponseEntity.ok("Login done");
		}else {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Login fail");
		}
	}

}
