package dn.batch.reactproject.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import dn.batch.reactproject.model.Product;
import dn.batch.reactproject.repository.ProductRepo;

@CrossOrigin
@RestController
@RequestMapping("/")
public class ProductController {
	
	@Autowired
	ProductRepo productRepo;
	
	@GetMapping("/keyboard")
	public List<Product> getProducts(){
		List<Product> products = productRepo.findAll();
		List<Product> keyboardList = new ArrayList<>();
			for(Product keyboard : products) {
			String productType = keyboard.getProductCategoryType();
			if("keyboard".equalsIgnoreCase(productType)) {
				keyboardList.add(keyboard);
			}
		}
			return keyboardList;
	}
	
	@GetMapping("/keycap")
	public List<Product> getList(){
		List<Product> product = productRepo.findAll();
		List<Product>keycapList = new ArrayList<>();
		for(Product keycap : product) {
			String productTypeString =  keycap.getProductCategoryType();
			if("keycap".equalsIgnoreCase(productTypeString)) {
				keycapList.add(keycap);
			}
		}
		return keycapList;
	}

}
