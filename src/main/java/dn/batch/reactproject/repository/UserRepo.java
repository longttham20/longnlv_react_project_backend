package dn.batch.reactproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import dn.batch.reactproject.model.User;

@Repository
public interface UserRepo extends JpaRepository<User, Integer>{
	User findByLoginUser(String loginUser);
}
