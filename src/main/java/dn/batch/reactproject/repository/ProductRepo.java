package dn.batch.reactproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import dn.batch.reactproject.model.Product;

@Repository
public interface ProductRepo extends JpaRepository<Product, Integer> {

}
