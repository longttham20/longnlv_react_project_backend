package dn.batch.reactproject.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "tb_product")
public class Product {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idProduct")
	private int idProduct;
	
	@Column(name = "nameProduct")
	private String nameProduct;
	
	@Column(name = "sale")
	private int sale;
	
	@Column(name = "src")
	private String src;
	
	@Column(name = "priceProduct")
	private int priceProduct;
	
	@ManyToOne
	@JoinColumn(name = "idPC")
	@JsonBackReference
	private ProductCategory idPCategory;
	
	@OneToMany(mappedBy = "idProduct")
	@JsonManagedReference
	List<CartDetail> cartDetails;

	public int getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(int idProduct) {
		this.idProduct = idProduct;
	}

	public String getNameProduct() {
		return nameProduct;
	}

	public void setNameProduct(String nameProduct) {
		this.nameProduct = nameProduct;
	}

	public int getSale() {
		return sale;
	}

	public void setSale(int sale) {
		this.sale = sale;
	}

	public String getSrc() {
		return src;
	}

	public void setSrc(String src) {
		this.src = src;
	}

	public int getPriceProduct() {
		return priceProduct;
	}

	public void setPriceProduct(int priceProduct) {
		this.priceProduct = priceProduct;
	}

	public ProductCategory getIdPCategory() {
		return idPCategory;
	}

	public void setIdPCategory(ProductCategory idPCategory) {
		this.idPCategory = idPCategory;
	}

	public List<CartDetail> getCartDetails() {
		return cartDetails;
	}

	public void setCartDetails(List<CartDetail> cartDetails) {
		this.cartDetails = cartDetails;
	}

	@Override
	public String toString() {
		return "Product [idProduct=" + idProduct + ", nameProduct=" + nameProduct + ", sale=" + sale + ", src=" + src
				+ ", priceProduct=" + priceProduct + ", idPCategory=" + idPCategory + ", cartDetails=" + cartDetails
				+ "]";
	}
	
	public String getProductCategoryType() {
		return this.idPCategory.getNameProductCategory();
	}
	
}
