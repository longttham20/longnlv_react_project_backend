package dn.batch.reactproject.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "tb_cartdetail")
public class CartDetail {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idCartDetail")
	private int idCartDetail;
	
	@ManyToOne
	@JoinColumn(name = "idCart")
	@JsonBackReference
	private Cart idCart;
	
	@ManyToOne
	@JoinColumn(name = "idProduct")
	@JsonBackReference
	private Product idProduct;
	
	@Column(name = "qty")
	private int qty;
	
	@Column(name = "price")
	private int price;

	public int getIdCartDetail() {
		return idCartDetail;
	}

	public void setIdCartDetail(int idCartDetail) {
		this.idCartDetail = idCartDetail;
	}

	public Cart getIdCart() {
		return idCart;
	}

	public void setIdCart(Cart idCart) {
		this.idCart = idCart;
	}

	public Product getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(Product idProduct) {
		this.idProduct = idProduct;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "CartDetail [idCartDetail=" + idCartDetail + ", idCart=" + idCart + ", idProduct=" + idProduct + ", qty="
				+ qty + ", price=" + price + "]";
	}

	
}
