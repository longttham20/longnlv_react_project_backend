package dn.batch.reactproject.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "tb_productcategory")
public class ProductCategory {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idPC")
	private int idProductCategory;
	
	@Column(name = "namePC")
	private String nameProductCategory;
	
	@OneToMany(mappedBy = "idPCategory" )
	@JsonManagedReference
	private List<Product> products;

	public int getIdProductCategory() {
		return idProductCategory;
	}

	public void setIdProductCategory(int idProductCategory) {
		this.idProductCategory = idProductCategory;
	}

	public String getNameProductCategory() {
		return nameProductCategory;
	}

	public void setNameProductCategory(String nameProductCategory) {
		this.nameProductCategory = nameProductCategory;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	@Override
	public String toString() {
		return "ProductCategory [idProductCategory=" + idProductCategory + ", nameProductCategory="
				+ nameProductCategory + ", products=" + products + "]";
	}
	
	
}
