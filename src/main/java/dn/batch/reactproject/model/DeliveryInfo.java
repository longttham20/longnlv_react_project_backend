package dn.batch.reactproject.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "tb_delivery")
public class DeliveryInfo {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idDelivery")
	private int idDelivery;
	
	@Column(name = "nameDelivery")
	private String nameDelivery;
	
	@Column(name = "phone")
	private String phone;
	
	@Column(name = "address")
	private String address;
	
	@ManyToOne
	@JoinColumn(name = "idUser")
	@JsonBackReference
	private User  idUserDeli;
	
	@OneToMany(mappedBy = "idDeliveryCart")
	@JsonManagedReference
	List<Cart> carts;

	public int getIdDelivery() {
		return idDelivery;
	}

	public void setIdDelivery(int idDelivery) {
		this.idDelivery = idDelivery;
	}

	public String getNameDelivery() {
		return nameDelivery;
	}

	public void setNameDelivery(String nameDelivery) {
		this.nameDelivery = nameDelivery;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public User getIdUserDeli() {
		return idUserDeli;
	}

	public void setIdUserDeli(User idUserDeli) {
		this.idUserDeli = idUserDeli;
	}

	public List<Cart> getCarts() {
		return carts;
	}

	public void setCarts(List<Cart> carts) {
		this.carts = carts;
	}

	@Override
	public String toString() {
		return "DeliveryInfo [idDelivery=" + idDelivery + ", nameDelivery=" + nameDelivery + ", phone=" + phone
				+ ", address=" + address + ", idUserDeli=" + idUserDeli + ", carts=" + carts + "]";
	}
	
	
}
