package dn.batch.reactproject.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "tb_user")
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idUser")
	private int  idUser;
	
	@Column(name = "name")
	private String nameUser;
	
	@Column(name = "userlogin")
	private String loginUser;
	
	@Column(name = "password")
	private String passwordUser;
	
	@OneToMany(mappedBy = "idUserCart")
	@JsonManagedReference
	private List<Cart> carts;
	
	@OneToMany(mappedBy = "idUserDeli")
	@JsonManagedReference
	private List<DeliveryInfo> deliveryInfos;

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String getNameUser() {
		return nameUser;
	}

	public void setNameUser(String nameUser) {
		this.nameUser = nameUser;
	}

	public String getLoginUser() {
		return loginUser;
	}

	public void setLoginUser(String loginUser) {
		this.loginUser = loginUser;
	}

	public String getPasswordUser() {
		return passwordUser;
	}

	public void setPasswordUser(String passwordUser) {
		this.passwordUser = passwordUser;
	}

	public List<Cart> getCarts() {
		return carts;
	}

	public void setCarts(List<Cart> carts) {
		this.carts = carts;
	}

	public List<DeliveryInfo> getDeliveryInfos() {
		return deliveryInfos;
	}

	public void setDeliveryInfos(List<DeliveryInfo> deliveryInfos) {
		this.deliveryInfos = deliveryInfos;
	}

	@Override
	public String toString() {
		return "User [idUser=" + idUser + ", nameUser=" + nameUser + ", loginUser=" + loginUser + ", passwordUser="
				+ passwordUser + ", carts=" + carts + ", deliveryInfos=" + deliveryInfos + "]";
	}
	
	
}
