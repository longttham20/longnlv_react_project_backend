package dn.batch.reactproject.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "tb_cart")
public class Cart {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idCart")
	private int idCart;
	
	@ManyToOne
	@JoinColumn(name = "idDelivery")
	@JsonBackReference
	private DeliveryInfo idDeliveryCart;
	
	@ManyToOne
	@JoinColumn(name = "idUser")
	@JsonBackReference
	private User idUserCart;
	
	@Column(name = "total")
	private int total;
	
	@OneToMany(mappedBy = "idCart")
	@JsonManagedReference
	private List<CartDetail> cartDetails;

	public int getIdCart() {
		return idCart;
	}

	public void setIdCart(int idCart) {
		this.idCart = idCart;
	}

	public DeliveryInfo getIdDelivery() {
		return idDeliveryCart;
	}

	public void setIdDelivery(DeliveryInfo idDelivery) {
		this.idDeliveryCart = idDelivery;
	}

	public User getIdUserCart() {
		return idUserCart;
	}

	public void setIdUserCart(User idUserCart) {
		this.idUserCart = idUserCart;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List<CartDetail> getCartDetails() {
		return cartDetails;
	}

	public void setCartDetails(List<CartDetail> cartDetails) {
		this.cartDetails = cartDetails;
	}

	@Override
	public String toString() {
		return "Cart [idCart=" + idCart + ", idDelivery=" + idDeliveryCart + ", idUserCart=" + idUserCart + ", total="
				+ total + ", cartDetails=" + cartDetails + "]";
	}

	
}
